﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileDownloadExample.Models
{
    public class IndexViewModel
    {
        public IEnumerable<string> Files { get; set; }
    }
}